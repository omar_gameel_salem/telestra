package org.omarsalem.telestra.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhoneNumber {
    private UUID id;
    private UUID customerId;
    private NumberType type;
    private String number;
    private boolean active;
}
