package org.omarsalem.telestra.models;

public enum NumberType {
    HOME, WORK, MOBILE
}
