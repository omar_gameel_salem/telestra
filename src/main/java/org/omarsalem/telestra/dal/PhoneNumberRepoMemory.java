package org.omarsalem.telestra.dal;

import org.omarsalem.telestra.models.NotfoundException;
import org.omarsalem.telestra.models.NumberType;
import org.omarsalem.telestra.models.PhoneNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class PhoneNumberRepoMemory implements PhoneNumberRepo {
    private static final List<PhoneNumber> PHONE_NUMBERS;

    private static final UUID FIRST_CUSTOMER = UUID.fromString("d6d42676-af69-48dc-a3c2-2abd52feca7f");
    private static final UUID SECOND_CUSTOMER = UUID.fromString("0f83b43d-1728-4a11-8cc2-71bfef5548db");
    private static final UUID PHONE_ID = UUID.fromString("0f83b43d-1728-4a11-8cc2-71bfef5548db");

    static {
        PHONE_NUMBERS = new ArrayList<>();
        PHONE_NUMBERS.add(new PhoneNumber(UUID.randomUUID(), FIRST_CUSTOMER, NumberType.MOBILE, "0435919848", false));
        PHONE_NUMBERS.add(new PhoneNumber(UUID.randomUUID(), FIRST_CUSTOMER, NumberType.MOBILE, "04359858535", false));
        PHONE_NUMBERS.add(new PhoneNumber(PHONE_ID, SECOND_CUSTOMER, NumberType.MOBILE, "04985853544", false));
    }

    @Override
    public List<PhoneNumber> findAll() {
        return PHONE_NUMBERS;
    }

    @Override
    public List<PhoneNumber> findByCustomerId(UUID customerId) {
        return PHONE_NUMBERS
                .stream()
                .filter(phoneNumber -> customerId.equals(phoneNumber.getCustomerId()))
                .collect(Collectors.toList());
    }

    @Override
    public void activateNumber(UUID id) {
        final PhoneNumber number = findById(id);
        number.setActive(true);

    }

    public PhoneNumber findById(UUID id) {
        return PHONE_NUMBERS
                .stream()
                .filter(phoneNumber -> id.equals(phoneNumber.getId()))
                .findAny()
                .orElseThrow(NotfoundException::new);
    }
}
