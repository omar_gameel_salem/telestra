package org.omarsalem.telestra.dal;

import org.omarsalem.telestra.models.PhoneNumber;

import java.util.List;
import java.util.UUID;

public interface PhoneNumberRepo {
    List<PhoneNumber> findAll();

    List<PhoneNumber> findByCustomerId(UUID customerId);

    void activateNumber(UUID id);
}
