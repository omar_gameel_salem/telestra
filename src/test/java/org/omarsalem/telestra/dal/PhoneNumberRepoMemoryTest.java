package org.omarsalem.telestra.dal;

import org.junit.Assert;
import org.junit.Test;
import org.omarsalem.telestra.models.NotfoundException;
import org.omarsalem.telestra.models.PhoneNumber;

import java.util.List;
import java.util.UUID;

public class PhoneNumberRepoMemoryTest {
    private final PhoneNumberRepoMemory target = new PhoneNumberRepoMemory();

    @Test
    public void findAll() {
        //Act
        final List<PhoneNumber> phoneNumberList = target.findAll();

        //Assert
        Assert.assertEquals(3, phoneNumberList.size());
    }

    @Test
    public void findByCustomerId_found() {
        //Arrange
        final UUID customerId = UUID.fromString("d6d42676-af69-48dc-a3c2-2abd52feca7f");

        //Act
        final List<PhoneNumber> phoneNumberList = target.findByCustomerId(customerId);

        //Assert
        Assert.assertEquals(2, phoneNumberList.size());
    }

    @Test
    public void findByCustomerId_notFound_returnsEmptyList() {
        //Arrange
        final UUID customerId = UUID.randomUUID();

        //Act
        final List<PhoneNumber> phoneNumberList = target.findByCustomerId(customerId);

        //Assert
        Assert.assertEquals(0, phoneNumberList.size());
    }

    @Test
    public void activateNumber() {
        //Arrange
        final UUID phoneId = UUID.fromString("0f83b43d-1728-4a11-8cc2-71bfef5548db");

        //Act
        target.activateNumber(phoneId);

        //Assert
        Assert.assertTrue(target.findById(phoneId).isActive());
    }

    @Test(expected = NotfoundException.class)
    public void activateNumber_notFound_throwsException() {
        //Arrange
        final UUID phoneId = UUID.randomUUID();

        //Act
        target.activateNumber(phoneId);
    }
}